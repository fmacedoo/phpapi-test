<?php

require '../vendor/autoload.php';
require '../lib/util.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// Changes the content type to JSON type
$app->response()->header('Content-Type', 'application/json;charset=utf-8');

$app->post('/', function () use ($app) {
	
	$link = "http://travellogix.api.test.conceptsol.com/api/Ticket/Search";
	$fields = $app->request()->post();

	// If exists auth parameter, i put that in the header as Authorization parameter.
	$post_response = null;
	if ($fields['auth']) {
		$headers = array(
		    'Authorization: '.$fields['auth']
		);

		$post_response = post($link, $fields, $headers);
	} else {
		// if not, i pass null to headers and true to chunk
		$post_response = post($link, $fields, null, true);
	}
	
	if ($post_response['statusCode'] == "401") {
		echo json_encode("You are not logged in.");
	} else {
		echo json_encode($post_response['output']);
	}

});

$app->post('/login', function () use ($app) {
	$fields = $app->request()->post();
	$post_response = post("http://travellogix.api.test.conceptsol.com/Token", $fields);
});

$app->run();