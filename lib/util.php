<?php

function post($link, $post_data, $headers = null, $isChunk = false) {
	$writefn = function($ch, $chunk) { 
		static $data='';
		static $limit = 500;

		$len = strlen($data) + strlen($chunk);
		if ($len >= $limit ) {
			$data .= substr($chunk, 0, $limit-strlen($data));
			echo strlen($data) , ' ', $data;
			return -1;
		}

		$data .= $chunk;
		return strlen($chunk);
	};

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $link);

	if ($isChunk) {
		curl_setopt($ch, CURLOPT_RANGE, '0-500');
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($ch, CURLOPT_WRITEFUNCTION, $writefn);
	}

	if ($headers) {
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	}

	if ($post_data) {
		$post_data_query = http_build_query($post_data);
		curl_setopt($ch, CURLOPT_POST, count($post_data));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data_query);
	}

	$output = curl_exec($ch);
	$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);

	return array(
		'output' => $output,
		'statusCode' => $statusCode
	);
}
